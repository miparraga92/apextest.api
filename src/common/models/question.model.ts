import {Prop} from "@nestjs/mongoose";
import {Document} from "mongoose";

export class QuestionModel extends Document {
    @Prop()
    index: string;
    @Prop()
    value: string;
}
