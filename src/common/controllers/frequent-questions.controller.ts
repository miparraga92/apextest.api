import {Controller, Get, Put} from '@nestjs/common';
import {ApiOperation, ApiTags} from "@nestjs/swagger";
import {ExtractorService} from "../services/extractor.service";
import {QuestionListDto} from "../dtos/question-list.dto";
import {QuestionListService} from "../services/question-list.service";

@ApiTags('frequent-questions')
@Controller('frequent-questions')
export class FrequentQuestionsController {
    constructor(private readonly extractorService: ExtractorService, private questionListService: QuestionListService) {
    }

    @ApiOperation({ description: "Extrae el contenido desde la pagina configurada, sin guardar" })
    @Get('extract')
    async getExtract(): Promise<QuestionListDto> {
        return await this.extractorService.extract();
    }

    @ApiOperation({ description: "Extrae el contenido desde la pagina configurada y guarda en la base MongoDB y respaldo el archivo Json configurado" })
    @Put('update')
    async updateQuestions(): Promise<void> {
        await this.questionListService.updateAll();
    }

    @ApiOperation({ description: "Extrae el contenido desde la pagina configurada y guarda en la base MongoDB" })
    @Put('mongo')
    async updateMongoQuestions(): Promise<void> {
        await this.questionListService.updateMongo();
    }

    @ApiOperation({ description: "Extrae el contenido desde la pagina configurada y guarda en el archivo Json configurado" })
    @Put('json')
    async updateJsonQuestions(): Promise<void> {
        await this.questionListService.updateJson();
    }

    @ApiOperation({ description: "Obtiene las preguntas guardadas en la base MongoDB" })
    @Get('mongo')
    async getMongoQuestions(): Promise<QuestionListDto> {
        return await this.questionListService.getFromMongo();
    }

    @ApiOperation({ description: "Obtiene las preguntas guardadas en el archivo Json" })
    @Get('json')
    async getJsonQuestions(): Promise<QuestionListDto> {
        return await this.questionListService.getFromJson();
    }
}
