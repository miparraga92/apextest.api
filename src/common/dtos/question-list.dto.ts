import {QuestionDto} from "./question.dto";

export class QuestionListDto {
    result: QuestionDto[];
    updated: string;
}
