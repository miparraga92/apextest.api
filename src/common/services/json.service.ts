import {Injectable} from '@nestjs/common';
import * as fs from "fs";

@Injectable()
export class JsonService {

    public async get<T>(path: string): Promise<T> {
        if (!fs.existsSync(path)) {
            return null;
        }
        return new Promise<T>((resolve, reject) => {
            fs.readFile(path, function (err, data) {
                if (err) throw err;
                const result: T = JSON.parse(data.toString());
                resolve(result);
                return;
            }), (err) => reject(err)
        })
    }

    public async save(path: string, data: any): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            fs.writeFile(path, JSON.stringify(data), err => {
                if (err) throw err;
                resolve();
            }), (err) => reject(err)
        })
    }
}
