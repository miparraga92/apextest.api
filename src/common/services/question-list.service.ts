import {Injectable} from "@nestjs/common";
import {QuestionListRepository} from "../repositories/question-list.repository";
import {ExtractorService} from "./extractor.service";
import {QuestionListDto} from "../dtos/question-list.dto";
import {QuestionDto} from "../dtos/question.dto";
import {QuestionListModel} from "../models/question-list.model";
import {JsonService} from "./json.service";
import {EasyconfigService} from "nestjs-easyconfig";

@Injectable()
export class QuestionListService {
    constructor(
        private repository: QuestionListRepository,
        private extractorService: ExtractorService,
        private jsonService: JsonService,
        private easyconfigService: EasyconfigService) {
    }

    async updateMongo(questionListDto?: QuestionListDto): Promise<void> {
        if(!questionListDto) questionListDto = await this.extractorService.extract();
        await this.repository.deleteAll();
        await this.repository.insert(questionListDto);
    }

    async updateJson(questionListDto?: QuestionListDto): Promise<void> {
        if(!questionListDto) questionListDto = await this.extractorService.extract();
        try {
            await this.jsonService.save(this.easyconfigService.get('PATH_JSON'), questionListDto)
        }catch (e) {
            console.error(e);
        }
    }

    async updateAll(): Promise<void> {
        const questionListDto: QuestionListDto = await this.extractorService.extract();
        await this.updateMongo(questionListDto);
        await this.updateJson(questionListDto);
    }

    async getFromMongo(): Promise<QuestionListDto> {
        let result = new QuestionListDto();
        const questionListModels = await this.repository.all();
        if (questionListModels && questionListModels.length > 0) {
            result = this.mapQuestionList(questionListModels[0]);
        }
        return result;
    }

    async getFromJson(): Promise<QuestionListDto> {
        let result = new QuestionListDto();
        try {
            result = await this.jsonService.get<QuestionListDto>(this.easyconfigService.get('PATH_JSON'));
        }catch (e) {
            console.error(e);
        }
        return result;
    }

    mapQuestionList(questionList: QuestionListModel): QuestionListDto {
        const result = new QuestionListDto();
        result.updated = questionList.updated;
        result.result = questionList.result.reduce((res, current) => {
            const questionDto = new QuestionDto();
            questionDto.index = current.index;
            questionDto.value = current.value;
            res.push(questionDto);
            return res;
        }, []);
        return result;
    }
}
